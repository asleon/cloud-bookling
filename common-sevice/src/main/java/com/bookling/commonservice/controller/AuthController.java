package com.bookling.commonservice.controller;

import com.bookling.commonservice.api.Constants;
import com.bookling.commonservice.dto.AuthDTO;
import com.bookling.commonservice.dto.JwtResponseDTO;
import com.bookling.commonservice.dto.exception.ErrorResponse;
import com.bookling.commonservice.security.jwt.JwtProvider;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping(Constants.API.PATH)
public class AuthController {
    private AuthenticationManager authenticationManager;
    private JwtProvider jwtProvider;

    @ApiOperation("Login user")
    @PostMapping(Constants.API.LOGIN)
    public ResponseEntity<?> login(@ApiParam("Auth request") @Valid @RequestBody AuthDTO request) throws Exception {
        try {
            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(auth);
            String jwt = jwtProvider.generateJwtToken(auth);
            UserDetails details = (UserDetails) auth.getPrincipal();
            return ResponseEntity.ok(new JwtResponseDTO(jwt,details.getUsername(),details.getAuthorities()));
        } catch (Exception ex) {
            log.error("Error in authorization", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.UNAUTHORIZED));
        }

    }

}
