package com.bookling.commonservice.controller;

import com.bookling.commonservice.api.Constants;
import com.bookling.commonservice.dto.BookDTO;
import com.bookling.commonservice.dto.PageDataDTO;
import com.bookling.commonservice.dto.exception.ErrorResponse;
import com.bookling.commonservice.service.BookService;
import com.bookling.commonservice.state.Book;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(Constants.API.PATH)
public class BookController {
    private final static int PAGE_LIMIT = 50;
    private final BookService bookService;

    @ApiOperation("Find all Books")
    @GetMapping(Constants.API.BOOKS)
    @SuppressWarnings("unchecked")
    public ResponseEntity<?> findAllBooks() {
        try {
            List<BookDTO> items = bookService.findAll()
                    .stream()
                    .limit(PAGE_LIMIT)
                    .map(book -> new BookDTO(book.getId(), book.getName(), book.getDescription()))
                    .collect(Collectors.toList());
            PageDataDTO<BookDTO> itemList = new PageDataDTO(items, bookService.count() == items.size());
            return new ResponseEntity<>(itemList, HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in load all books", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Find book by id")
    @GetMapping(Constants.API.BOOK)
    public ResponseEntity<?> findBookById(@ApiParam("id") @RequestParam String id) {
        try {
            Book book = bookService.findById(UUID.fromString(id));
            return new ResponseEntity<>(new BookDTO(book.getId(), book.getName(), book.getDescription()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in find book", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Create book entity")
    @PostMapping(Constants.API.BOOK_CREATE)
    @PreAuthorize("hasRole('LIBRARY') or hasRole('ADMIN')")
    public ResponseEntity<?> createBook(@ApiParam("Book entity") @Valid @RequestBody Book book) {
        try {
            Book genBook = bookService.create(book);
            return new ResponseEntity<>(new BookDTO(genBook.getId(), genBook.getName(), genBook.getDescription()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in create book", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Update book entity")
    @PutMapping(Constants.API.BOOK_UPDATE)
    @PreAuthorize("hasRole('LIBRARY') or hasRole('ADMIN')")
    public ResponseEntity<?> updateBook(@ApiParam("Book entity") @Valid @RequestBody Book book) {
        try {
            Book genBook = bookService.update(book);
            return new ResponseEntity<>(new BookDTO(genBook.getId(), genBook.getName(), genBook.getDescription()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in update book", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Delete book by id")
    @DeleteMapping(Constants.API.BOOK_DELETE)
    @PreAuthorize("hasRole('LIBRARY') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteBookById(@ApiParam("Book id") @RequestBody String id) {
        try {
            bookService.deleteById(UUID.fromString(id));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in delete book", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

}
