package com.bookling.commonservice.controller;

import com.bookling.commonservice.api.Constants;
import com.bookling.commonservice.dto.PageDataDTO;
import com.bookling.commonservice.dto.UserDTO;
import com.bookling.commonservice.dto.exception.ErrorResponse;
import com.bookling.commonservice.service.UserService;
import com.bookling.commonservice.state.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(Constants.API.PATH)
public class UserController {
    private final static int PAGE_LIMIT = 50;
    private final UserService userService;

    @ApiOperation("Find all USERS")
    @GetMapping(Constants.API.USERS)
    @PreAuthorize("hasRole('ADMIN')")
    @SuppressWarnings("unchecked")
    public ResponseEntity<?> findAllUsers() {
        try {
            List<UserDTO> items = userService.findAll()
                    .stream()
                    .limit(PAGE_LIMIT)
                    .map(user -> new UserDTO(user.getId(),
                            user.getName(),
                            user.getSurname(),
                            user.getUsername(),
                            user.getDescription(),
                            user.getEmail()))
                    .collect(Collectors.toList());
            PageDataDTO<UserDTO> itemList = new PageDataDTO(items, userService.count() == items.size());
            return new ResponseEntity<>(itemList, HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in load users", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Find USER by id")
    @GetMapping(Constants.API.USER)
    @PreAuthorize("hasRole('LIBRARY') or hasRole('ADMIN')")
    public ResponseEntity<?> findUserById(@ApiParam("id") @RequestParam String id) {
        try {
        User user = userService.findById(UUID.fromString(id));
        return new ResponseEntity<>(new UserDTO(user.getId(),
                user.getName(),
                user.getSurname(),
                user.getUsername(),
                user.getDescription(),
                user.getEmail()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in find user", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Create USER entity")
    @PostMapping(Constants.API.USER_CREATE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createUser(@ApiParam("User entity") @Valid @RequestBody User userEntity) {
        try {
        User user = userService.create(userEntity);
        return new ResponseEntity<>(new UserDTO(user.getId(),
                user.getName(),
                user.getSurname(),
                user.getUsername(),
                user.getDescription(),
                user.getEmail()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in create user", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Update USER entity")
    @PutMapping(Constants.API.USER_UPDATE)
    @PreAuthorize("hasRole('LIBRARY') or hasRole('ADMIN')")
    public ResponseEntity<?> updateUser(@ApiParam("User entity") @Valid @RequestBody User userEntity) {
        try {
        User user = userService.update(userEntity);
        return new ResponseEntity<>(new UserDTO(user.getId(),
                user.getName(),
                user.getSurname(),
                user.getUsername(),
                user.getDescription(),
                user.getEmail()), HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in update user", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }

    @ApiOperation("Delete USER by id")
    @DeleteMapping(Constants.API.USER_DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUserById(@ApiParam("id") @RequestBody String id) {
        try {
        userService.deleteById(UUID.fromString(id));
        return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Error in delete user", ex);
            return ResponseEntity.badRequest().body(new ErrorResponse(Constants.Exception.BAD_REQUEST));
        }
    }
}
