package com.bookling.commonservice.repo;

import com.bookling.commonservice.state.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepo extends JpaRepository<User, UUID> {
    User findUserByUsernameAndPassword(String username, String password);
    Optional<User> findUserByUsername(String username);
}
