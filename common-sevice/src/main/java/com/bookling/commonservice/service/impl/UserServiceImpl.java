package com.bookling.commonservice.service.impl;

import com.bookling.commonservice.repo.UserRepo;
import com.bookling.commonservice.service.UserService;
import com.bookling.commonservice.state.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepo repository;

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User findById(UUID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return repository.findUserByUsername(username);
    }

    @Override
    public User create(User user) {
        return repository.save(user);
    }

    @Override
    public User update(User user) {
        return repository.save(user);
    }

    @Override
    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    @Override
    public UUID findUserByUsernameAndPassword(String username, String password) {
        User user = repository.findUserByUsernameAndPassword(username, password);
        return user == null ? null : user.getId();
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = repository.findUserByUsername(name).orElseThrow(
                () -> new UsernameNotFoundException("User Not Found with -> username or email : " + name));

        return UserPrincipleImpl.build(user);
    }

}
