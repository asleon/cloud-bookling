package com.bookling.commonservice.service;

import com.bookling.commonservice.state.Book;

import java.util.List;
import java.util.UUID;

public interface BookService {
    List<Book> findAll();
    Book findById(UUID id);
    Book create(Book book);
    Book update(Book book);
    void deleteById(UUID id);
    long count();
}
