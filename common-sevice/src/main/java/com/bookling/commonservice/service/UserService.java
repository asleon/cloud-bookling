package com.bookling.commonservice.service;

import com.bookling.commonservice.state.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService extends UserDetailsService {
    List<User> findAll();
    User findById(UUID id);
    Optional<User> findUserByUsername(String username);
    User create(User user);
    User update(User user);
    void deleteById(UUID id);
    UUID findUserByUsernameAndPassword(String username, String password);
    long count();
}
