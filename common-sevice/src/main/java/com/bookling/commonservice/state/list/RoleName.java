package com.bookling.commonservice.state.list;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_LIBRARY,
    ROLE_USER
}
