package com.bookling.commonservice.api;

public interface Constants {
    interface API {
        String SLASH = "/",
                VERSION = "v1",
                PATH = SLASH + "api" + SLASH + VERSION,
                BOOK = SLASH + "book",
                BOOKS = BOOK + "s",
                BOOK_CREATE = BOOK + SLASH + "create",
                BOOK_UPDATE = BOOK + SLASH + "update",
                BOOK_DELETE = BOOK + SLASH + "delete",
                USER = SLASH + "user",
                USERS = USER + "s",
                USER_CREATE = USER + SLASH + "create",
                USER_UPDATE = USER + SLASH + "update",
                USER_DELETE = USER + SLASH + "delete";

        String AUTH = PATH + SLASH + "auth",
                AUTH_ALL = AUTH + SLASH + "**",
                LOGIN = "auth" + SLASH + "login",
                LOGOUT = "auth" + SLASH + "logout";

    }

    interface Exception {
        String EMPTY_FIELD = "The value should not be empty!",
                UNAUTHORIZED = "You have no access to a page!",
                BAD_REQUEST = "Something wrong. Please contact your administrator.";
    }
}
