package com.bookling.commonservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@AllArgsConstructor
@Getter
@ToString
public class UserDTO {
    private UUID id;
    private String name;
    private String surname;
    private String username;
    private String description;
    private String email;
}
