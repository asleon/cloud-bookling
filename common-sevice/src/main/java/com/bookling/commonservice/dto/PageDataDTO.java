package com.bookling.commonservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class PageDataDTO<T> {
    private List<T> items;
    private boolean isAllDataLoad;
}
