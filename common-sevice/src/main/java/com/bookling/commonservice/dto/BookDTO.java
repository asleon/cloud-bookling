package com.bookling.commonservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@AllArgsConstructor
@Getter
@ToString
public class BookDTO {
   private UUID id;
   private String name;
   private String description;
}
