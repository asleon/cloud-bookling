package com.bookling.commonservice.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
public class AuthDTO {
    @NotBlank
    @Size(min=3, max = 60)
    private String username;
    @NotBlank
    @Size(min=3, max = 40)
    private String password;
}
